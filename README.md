## Setting up

### 파이어베이스 프로젝트 만들기
  1. [파이어베이스 개발자 콘솔](https://console.firebase.google.com)을 이용해 파이어베이스 프로젝트를 만든다.
  1. 파이어베이스 요금제를 **Blaze** plan 으로 변경한다. Spart plan(무료)에서는 Cloud Function에서 Google 서비스 외부로 HTTP 요청을 보낼 수 없음.
  1. Include [Crashlytics in your project](https://firebase.google.com/docs/crashlytics/get-started).

### Firebase Function에 Function 추가
  1. Firebase CLI 설치. `npm install -g firebase-tools` 
  1. Firebase 로그인. `firebase login`.
  1. Firebase 프로젝트에 Function 추가`firebase use --add` 
  1. 의존성 설치 `cd functions; npm install;`
  
### Setting up an Incoming Jandi Webhook
  1. [incoming webhook 연동](https://support.jandi.com/hc/ko/articles/210952203-%EC%9E%94%EB%94%94-%EC%BB%A4%EB%84%A5%ED%8A%B8-%EC%9D%B8%EC%BB%A4%EB%B0%8D-%EC%9B%B9%ED%9B%85-Incoming-Webhook-%EC%9C%BC%EB%A1%9C-%EC%99%B8%EB%B6%80-%EB%8D%B0%EC%9D%B4%ED%84%B0%EB%A5%BC-%EC%9E%94%EB%94%94-%EB%A9%94%EC%8B%9C%EC%A7%80%EB%A1%9C-%EC%88%98%EC%8B%A0%ED%95%98%EA%B8%B0). **Webhook URL**을 복사해둔다.
  1. webhook URL Firebase 환경변수 설정: `firebase functions:config:set jandi.webhook_url="https://wh.jandi.com/connect-api/webhook/...` 

   
## Deploy and test

 1. 프로젝트에 배포 `firebase deploy`
 1. Simulate a test crash. [Instructions](https://firebase.google.com/docs/crashlytics/force-a-crash)


## License

© Google, 2017. Licensed under an [Apache-2](../LICENSE) license.
